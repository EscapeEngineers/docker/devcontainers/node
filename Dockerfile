#-------------------------------------------------------------------------------------------------------------
# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT License. See https://go.microsoft.com/fwlink/?linkid=2090316 for license information.
#-------------------------------------------------------------------------------------------------------------
ARG BASE_VARIANT

FROM --platform=$TARGETPLATFORM mcr.microsoft.com/vscode/devcontainers/typescript-node:${BASE_VARIANT}

# [Optional] Uncomment this section to install additional OS packages.
RUN apt-get update && export DEBIAN_FRONTEND=noninteractive \
    && apt-get -y install --no-install-recommends git-lfs

# [Optional] Uncomment if you want to install more global node packages
# RUN su node -c "npm install -g <your-package-list -here>"

# Create directory for shared local packages to configure correct owners
RUN install -d -m 0755 -o node -g node /localPackages

# Prepare directory for GnuPG so that the permissions are set correctly.
RUN install -d -m 0700 -o node -g node /home/node/.gnupg

# Copy custom scripts
COPY postCreateNpm.sh /

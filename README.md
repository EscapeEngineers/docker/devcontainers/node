# Node Docker Images

This project contains definitions for custom docker images with NodeJS.
They provide the development environment for JavaScript/React applications.

## Available Tags

- `18`, `latest` (Node v18.x)
- `16` (Node v16.x)

## Available Platforms

All images are available for the following platforms:

* linux/amd64
* linux/arm64
